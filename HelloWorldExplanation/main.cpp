/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2015, 10:26 AM
 */

#include <cstdlib>
#include <iostream> 
// Library, Iostream 
//provides information about cout
//Iostream provides input and output information

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Create a program identifier (variable)
    string name;
    
    // The variable "name" stores data in memory
    // Assign text to the variable name
    // Use the assignment operator (=)
    name = "Railroad Master"; // name is assigned "Nathaniel Palafox"
    // The variable "name" stores the text "Nathaniel Palafox"
    
    // Outputting the variable "name"
    // No quotation marks
    cout << name;
    
    return 0;
    
}

