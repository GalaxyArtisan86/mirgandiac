/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 10, 2015, 10:52 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Please enter two test scores: "
        << endl;
    int testScore1;
    int testScore2;
    int numTests =2;
    
    cin >> testScore1 >> testScore2;
    
    // Calculate average of test scores
    // Avoid logic error by editing parenthesis
    // Use static cast to change data type of variable
    double avg = (testScore1 + testScore2) 
    / static_cast<double>(numTests);
    
    // Output 2 decimal places
    cout << fixed << setprecision(2);
    
    cout << "Avg: " << avg << endl;
    
    
    
    return 0;
}

