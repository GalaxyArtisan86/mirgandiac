/* 
 * File:   main.cpp
 * Author: Nathaniel Palafox
 *
 * Created on September 10, 2015, 11:08 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int main()
{
/*
 * 
 */
    cout << "Hello ";
    int number_of_pods, peas_per_pod, total_peas;
    
    cout << " Press return after entering a number. \n";
    cout << "Enter two numbers of pods: \n";
    
    int peaPod1;
    int peaPod2;
    int numPods =2;
    
    cin >> peaPod1 >> peaPod2;
    
    // Calculate average of pea pods
    // Avoid logic error by editing parenthesis
    // Use static cast to change data type of variable
    double avg = (peaPod1 + peaPod2)
    / static_cast<double>(numPods);
    
    // Output 2 decimal places
    cout << "Avg: " << avg <<endl;
    cout << "This is how many pea pods you have.\n";
    
    
    cout << "Good-bye\n";
    
    return 0;
}

